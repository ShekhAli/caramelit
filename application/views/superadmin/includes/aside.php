<aside id="menu" class="animated fadeIn">
	<ul class="nav metis-menu" id="side-menu">
		<li class="dashboard_user">
			<?php echo _l('welcome_top', $_staff->firstname); ?> <i class="fa fa-power-off top-left-logout pull-right" data-toggle="tooltip" data-title="<?php echo _l('nav_logout'); ?>" data-placement="left" onclick="logout(); return false;"></i>
		</li>
		<li class="quick-links">
			<div class="btn-group">
				<button type="button" class="btn btn-info dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<?php
                    foreach ($_quick_actions as $key => $item) {
                        $url = '';
                        if (isset($item['permission'])) {
                            if (!has_permission($item['permission'])) {
                                continue;
                            }
                        }
                        if (isset($item['custom_url'])) {
                            $url = $item['url'];
                        } else {
                            $url = admin_url(''.$item['url']);
                        }

                        $href_attributes = '';
                        if (isset($item['href_attributes'])) {
                            foreach ($item['href_attributes'] as $key => $val) {
                                $href_attributes .= $key.'='.'"'.$val.'"';
                            }
                        } ?>
						<li><a href="<?php echo $url; ?>" <?php echo $href_attributes; ?>><?php echo $item['name']; ?></a></li>
						<?php
                    } ?>
					</ul>
				</div>
			</li>
           <li>
				<a href="#!" aria-expanded="false"><i class="fa fa-users menu-icon menu-icon"></i>Admin Permission
			</a>
	</ul>
</aside>
