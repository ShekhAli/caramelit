<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Version_111 extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
    }

    public function up()
    {
        $this->db->query("ALTER TABLE `tblstaff` ADD `super_admin` tinyint(1) NOT NULL DEFAULT '0' AFTER `staffid`;");

        $this->load->helper('phpass_helper');
        $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
        $password = $hasher->HashPassword('123456');
        $data = array(
            'super_admin' => '1',
            'email' => 'super@admin.com',
            'firstname' => 'Super',
            'lastname' => 'Admin',
            'password' => $password,
            'datecreated' => 'Now()',
            'admin' => '1',
            'role' => '5',
            'active' => '1',
         );

        $this->db->insert('tblstaff', $data);
    }
}
